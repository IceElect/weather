import { useContext, useEffect } from "react";
import FavoriteList from "../components/FavoriteList";
import Weather from "../components/Weather"
import { WeatherContext } from "../context/Weather/WeatherContext";

const HomePage = (props) => {

    const { location, isLoading, setCity, clearLocation } = useContext(WeatherContext);

    useEffect(() => {
        
        if(!isLoading && !location.coords.lat)
            setCity('')

    }, [])

    return (
        <>
            <Weather needDesc={true} />
            <FavoriteList />
        </>
    )
}

export default HomePage