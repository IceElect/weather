import { useContext, useEffect } from "react";
import { useParams } from "react-router";
import Forecast from "../components/Forecast";
import Weather from "../components/Weather";
import { WeatherContext } from "../context/Weather/WeatherContext";

const ForecastPage = () => {

    const { city, period } = useParams()

    const { isLoading, setCity } = useContext(WeatherContext)

    useEffect(() => {
        if(!isLoading)
            setCity(city)
    }, [city])

    return (
        <> 
            <Weather />
            <Forecast city={city} period={period} />
        </>
    )
}

export default ForecastPage