import { useReducer } from "react"
import { WeatherContext, initialState, periodTypes } from "./WeatherContext"
import { weatherReducer } from "./WeatherReducer"
import { ADD_FAVORITE, HIDE_LOADER, LOADING_FORECAST, LOADING_WEATHER, REMOVE_FAVORITE, SET_CITY, SET_COORDS, SET_ERROR, SET_FORECAST, SET_PERIOD, SET_WEATHER, SHOW_LOADER } from "./types"

import axios from "axios"
import Geocode from "../../api/geocode"
import { monthNames, prepareForecastDaily, prepareForecastHourly, prepareHistoryDay } from "../../utils/weather"

export const WeatherState = ({children}) => {

    const [state, dispatch] = useReducer(weatherReducer, initialState)

    const { period } = state;
    
    const cDate = new Date();
    
    const periondsDays = {
        'now': [0, 0],
        'today': [0, 0],
        'tomorrow': [1, 1],
        'week': [1 - cDate.getDay() , 7 - cDate.getDay()]
    }
    

    const sDate = new Date();
          sDate.setDate(cDate.getDate() + periondsDays[period][0] || 0);
          sDate.setHours(0);
          sDate.setMinutes(0);

    const eDate = new Date();
          eDate.setDate(cDate.getDate() + periondsDays[period][1] || 0);
          eDate.setHours(23);
          eDate.setMinutes(0);

    const API_KEY = 'a2560ec717b9029e6290962eb38aa6ed'
    const API_URL = 'https://api.openweathermap.org/data/2.5'

    const setError = (error) => {
        dispatch({type: SET_ERROR, payload: error})
    }

    const setCity = async (city) => {
        dispatch({type: SET_ERROR, payload: false})
        dispatch({type: SET_CITY, payload: ''})

        if(!city && !state.isLoading) {
            dispatch({type: SHOW_LOADER})

            navigator.geolocation.getCurrentPosition(async ({coords}) => {
                let {status, results} = await Geocode.fromLatLng(coords.latitude, coords.longitude)
                if(status) {
                    let city = results[0].address_components
                                .filter((item) => item.types.includes("locality"))
                                .shift().long_name
                              
                    dispatch({type: SET_COORDS, payload: {lat: coords.latitude, lng: coords.longitude}})
                    dispatch({type: SET_CITY, payload: city})
                    dispatch({type: SET_WEATHER, payload: {}})
                    dispatch({type: HIDE_LOADER})
                }
            })
        } else if(city) {
            if(!state.error) {
                await Geocode.fromAddress(city).then(
                    response => {
                        const { lat, lng } = response.results[0].geometry.location;
                        let coords = {lat, lng}
                        dispatch({type: SET_CITY, payload: city})
                        dispatch({type: SET_COORDS, payload: coords})
                        dispatch({type: HIDE_LOADER})
                        return coords;
                    },
                    error => {
                        console.error(error);
                    }
                );
            }

            dispatch({type: SET_WEATHER, payload: {}})
        }
    }

    const fetchWeather = () => {
        console.log('fetchWeather');
        dispatch({type: LOADING_WEATHER, payload: true})
        // dispatch({type: SET_PERIOD, payload: period})

        let params = {
            appid: API_KEY,
            units: 'metric',
            q: state.location.city,
        }

        axios.get(API_URL + '/weather', {params}).then(({data: payload}) => {
            dispatch({type: SET_WEATHER, payload})
            dispatch({type: LOADING_WEATHER, payload: false})
        }).catch(error => {
            dispatch({type: SET_CITY, payload: false})
            // dispatch({type: SET_COORDS, payload: {}})
            dispatch({type: LOADING_WEATHER, payload: false})
            dispatch({type: LOADING_FORECAST, payload: false})
            dispatch({type: SET_ERROR, payload: 'City not found'})
        })
    }

    const fetchForecast = async (period) => {
        dispatch({type: LOADING_FORECAST, payload: true})
        dispatch({type: SET_PERIOD, payload: periodTypes[period] || periodTypes.DEFAULT})

        let {location: {city, coords}, error} = state;

        let params = {
            appid: API_KEY,
            units: 'metric',
            lat: coords.lat, 
            lon: coords.lng,
        }

        axios.get(API_URL + '/onecall', {params}).then(async ({data: payload}) => {
            
            if(period === 'week') {
                let delta = Math.abs(1 - cDate.getDay())

                for(let i = 1; i <= delta; i++) {
                    let nDate = new Date();
                        nDate.setDate(cDate.getDate() - i);
                        nDate.setHours(12);
                        nDate.setMinutes(0);
                        nDate.setSeconds(0);
                        
                        params.dt = nDate.getTime() / 1000 | 0;

                    let day = await axios.get(API_URL + '/onecall/timemachine', {params})
                    payload.daily.unshift(prepareHistoryDay(day.data))
                }
            }
            
            dispatch({type: SET_FORECAST, payload})
            dispatch({type: LOADING_FORECAST, payload: false})
        })
    }

    const clearLocation = () => {
        dispatch({type: SET_CITY, payload: ''})
        dispatch({type: SET_COORDS, payload: {}})
    }

    const getTemp = () => {
        return Math.round(state.weather.main?.temp || 0)
    }

    const getWeatherDesc = () => {
        const {weather, wind} = state.weather
        return `${weather[0].main}, Wind - ${wind.speed} meter per second`;
    }

    const getLocation = () => {
        return [
            state.location.city,
            state.weather.sys?.country
        ].filter(Boolean).join(', ')
    }

    const getPeriod = () => {
        const sMonth = monthNames[sDate.getMonth()];
        const sDay = String(sDate.getDate()).padStart(2, '0');

        const eMonth = monthNames[eDate.getMonth()];
        const eDay = String(eDate.getDate()).padStart(2, '0');

        return {
            'today': `${sMonth}, ${sDay}`,
            'tomorrow': `${sMonth}, ${sDay}`,
            'week': `${sMonth}, ${sDay} - ${eMonth}, ${eDay}`
        }[period]
    }

    const getForecastHourly = () => {
        prepareForecastHourly(state.forecast, sDate, eDate)
        return state.forecast.hourly
    }

    const getForecastDaily = () => {
        prepareForecastDaily(state.forecast, sDate, eDate)
        return state.forecast.daily
    }

    const toggleFavorite = (city) => {

        let favorites = localStorage.getItem('favorites')?.split(',') || [];
        let favoriteKey = favorites.indexOf(city);

        if(favoriteKey !== -1) {
            favorites.splice(favoriteKey, 1);
            dispatch({type: REMOVE_FAVORITE, payload: city})
        } else {
            favorites.push(city);
            dispatch({type: ADD_FAVORITE, payload: city})
        }

        localStorage.setItem('favorites', favorites);
        return favorites
    }

    return (
        <WeatherContext.Provider value={{
            ...state,
            getTemp, getWeatherDesc, getLocation, getPeriod, getForecastHourly, getForecastDaily,
            setCity, setError, fetchWeather, fetchForecast, clearLocation,
            toggleFavorite
        }}>
            {children}
        </WeatherContext.Provider>
    )
}