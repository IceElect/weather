import { createContext } from "react";

export const periodTypes = {
    now: 'now',
    today: 'today',
    tomorrow: 'tomorrow',
    week: 'week',
    DEFAULT: 'today',
}

export const initialState = {
    error: '',
    isLoading: false,
    loadingWeather: false,
    loadingForecast: false,

    period: periodTypes.now,
    location: {
        city: '',
        coords: {},
    },

    weather: {},
    forecast: {
        hourly: [],
        daily: []
    },

    favorites: localStorage.getItem('favorites')?.split(',') || [],
}

export const WeatherContext = createContext(initialState);