import { HIDE_LOADER, SET_FORECAST, SET_PERIOD, SET_WEATHER, SET_CITY, SET_COORDS, SHOW_LOADER, SET_ERROR, LOADING_WEATHER, LOADING_FORECAST, TOGGLE_FAVORITE, ADD_FAVORITE, REMOVE_FAVORITE } from "./types";

const handlers = {
    [SET_ERROR]: (state, {payload}) => ({...state, error: payload}),
    
    [SHOW_LOADER]: (state) => ({...state, isLoading: true}),
    [HIDE_LOADER]: (state) => ({...state, isLoading: false}),

    [LOADING_WEATHER]: (state, {payload}) => ({...state, loadingWeather: payload}),
    [LOADING_FORECAST]: (state, {payload}) => ({...state, loadingForecast: payload}),

    [SET_CITY]: (state, {payload}) => ({...state, location: {...state.location, city: payload}}),
    [SET_COORDS]: (state, {payload}) => ({...state, location: {...state.location, coords: payload}}),

    [SET_PERIOD]: (state, {payload}) => ({...state, period: payload}),
    [SET_WEATHER]: (state, {payload}) => ({...state, weather: payload}),
    [SET_FORECAST]: (state, {payload}) => ({...state, forecast: payload}),
    
    [ADD_FAVORITE]: (state, {payload}) => ({...state, favorites: [...state.favorites, payload]}),
    [REMOVE_FAVORITE]: (state, {payload}) => ({...state, favorites: state.favorites.filter(item => item !== payload)}),
    [TOGGLE_FAVORITE]: (state, {payload}) => (
        state.favorites.includes(payload)
            ? {...state, favorites: state.favorites.filter(item => item !== payload)}
            : {...state, favorites: [...state.favorites, payload]}
    ),

    DEFAULT: state => state
}

export const weatherReducer = (state, action) => {
    const handler = handlers[action.type] || handlers.DEFAULT;
    return handler(state, action)
}