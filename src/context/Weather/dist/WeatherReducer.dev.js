"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.weatherReducer = void 0;

var _types = require("./types");

var _handlers;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var handlers = (_handlers = {}, _defineProperty(_handlers, _types.SET_ERROR, function (state, _ref) {
  var payload = _ref.payload;
  return _objectSpread({}, state, {
    error: payload
  });
}), _defineProperty(_handlers, _types.SHOW_LOADER, function (state) {
  return _objectSpread({}, state, {
    isLoading: true
  });
}), _defineProperty(_handlers, _types.HIDE_LOADER, function (state) {
  return _objectSpread({}, state, {
    isLoading: false
  });
}), _defineProperty(_handlers, _types.LOADING_WEATHER, function (state, _ref2) {
  var payload = _ref2.payload;
  return _objectSpread({}, state, {
    loadingWeather: payload
  });
}), _defineProperty(_handlers, _types.LOADING_FORECAST, function (state, _ref3) {
  var payload = _ref3.payload;
  return _objectSpread({}, state, {
    loadingForecast: payload
  });
}), _defineProperty(_handlers, _types.SET_CITY, function (state, _ref4) {
  var payload = _ref4.payload;
  return _objectSpread({}, state, {
    location: _objectSpread({}, state.location, {
      city: payload
    })
  });
}), _defineProperty(_handlers, _types.SET_COORDS, function (state, _ref5) {
  var payload = _ref5.payload;
  return _objectSpread({}, state, {
    location: _objectSpread({}, state.location, {
      coords: payload
    })
  });
}), _defineProperty(_handlers, _types.SET_PERIOD, function (state, _ref6) {
  var payload = _ref6.payload;
  return _objectSpread({}, state, {
    period: payload
  });
}), _defineProperty(_handlers, _types.SET_WEATHER, function (state, _ref7) {
  var payload = _ref7.payload;
  return _objectSpread({}, state, {
    weather: payload
  });
}), _defineProperty(_handlers, _types.SET_FORECAST, function (state, _ref8) {
  var payload = _ref8.payload;
  return _objectSpread({}, state, {
    forecast: payload
  });
}), _defineProperty(_handlers, _types.ADD_FAVORITE, function (state, _ref9) {
  var payload = _ref9.payload;
  return _objectSpread({}, state, {
    favorites: [].concat(_toConsumableArray(state.favorites), [payload])
  });
}), _defineProperty(_handlers, _types.REMOVE_FAVORITE, function (state, _ref10) {
  var payload = _ref10.payload;
  return _objectSpread({}, state, {
    favorites: state.favorites.filter(function (item) {
      return item !== payload;
    })
  });
}), _defineProperty(_handlers, _types.TOGGLE_FAVORITE, function (state, _ref11) {
  var payload = _ref11.payload;
  return state.favorites.includes(payload) ? _objectSpread({}, state, {
    favorites: state.favorites.filter(function (item) {
      return item !== payload;
    })
  }) : _objectSpread({}, state, {
    favorites: [].concat(_toConsumableArray(state.favorites), [payload])
  });
}), _defineProperty(_handlers, "DEFAULT", function DEFAULT(state) {
  return state;
}), _handlers);

var weatherReducer = function weatherReducer(state, action) {
  var handler = handlers[action.type] || handlers.DEFAULT;
  return handler(state, action);
};

exports.weatherReducer = weatherReducer;