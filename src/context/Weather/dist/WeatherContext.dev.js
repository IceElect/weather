"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WeatherContext = exports.initialState = exports.periodTypes = void 0;

var _react = require("react");

var periodTypes = {
  now: 'now',
  today: 'today',
  tomorrow: 'tomorrow',
  week: 'week',
  DEFAULT: 'today'
};
exports.periodTypes = periodTypes;
var initialState = {
  error: '',
  isLoading: false,
  loadingWeather: false,
  loadingForecast: false,
  period: periodTypes.now,
  location: {
    city: '',
    coords: {}
  },
  weather: {},
  forecast: {
    hourly: [],
    daily: []
  },
  favorites: localStorage.getItem('favorites').split(',') || []
};
exports.initialState = initialState;
var WeatherContext = (0, _react.createContext)(initialState);
exports.WeatherContext = WeatherContext;