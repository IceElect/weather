import { useContext } from 'react';
import { Switch, Router, Route } from 'react-router-dom';
import { WeatherState } from '../context/Weather/WeatherState';
import history from './history';
import PageTitle from './PageTitle';
import routes from './routes';

const Layout = () => {
    return (
        <WeatherState>
            <Router history={history}>
                <Switch>
                    {Object.entries(routes).map(([route, {path, container: Cont, exact, title, data}]) => 
                        <Route 
                            key={route}
                            exact={exact}
                            path={path} 
                            component={props => (
                                <>
                                    <PageTitle title={title} />
                                    <Cont {...props} data={data} />
                                </>
                            )}
                        />
                    )}
                </Switch>
            </Router>
        </WeatherState>
    )
}

export default Layout