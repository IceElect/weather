"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _routes = _interopRequireDefault(require("./routes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var route = function route(name) {
  for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  if (_routes["default"][name]) {
    return _routes["default"][name].path.replace(/(:[a-z]+)+/g, function (match) {
      return args.shift();
    }).replace(/([\/]{2,})/g, '/');
  } else {
    return '';
  }
};

var _default = route;
exports["default"] = _default;