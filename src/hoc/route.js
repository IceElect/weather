import routes from "./routes";

const route = (name, ...args) => {
    if(routes[name]){
        return routes[name].path
                .replace(/(:[a-z]+)+/g, (match) => args.shift())
                .replace(/([\/]{2,})/g, '/')
    } else {
        return ''
    }
}

export default route