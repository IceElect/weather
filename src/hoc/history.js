// tslint:disable:interface-name
import { createBrowserHistory, Location } from "history";

const history = createBrowserHistory();
export default history;