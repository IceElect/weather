import { Route } from "react-router";
import DefaultContainer from "./containers/DefaultContainer";

const routes = {
    "home": {
        path: '/',
        container: DefaultContainer,
        title: "",
        exact: true
    },
    "period": {
        path: '/:period',
        container: DefaultContainer,
        title: '',
        exact: true
    },
    "periodByCity": {
        path: '/:city/:period',
        container: DefaultContainer,
        title: '',
        exact: true
    },
}

export default routes;