import { useContext } from "react";
import { Route } from "react-router";
import Header from "../../components/Header";
import { periodTypes, WeatherContext } from "../../context/Weather/WeatherContext";
import ForecastPage from "../../pages/ForecastPage";
import HomePage from "../../pages/HomePage";
import route from "../route";

const DefaultContainer = (props) => {

    const { isLoading, weather, error, fetchWeather, setError, setCity } = useContext(WeatherContext)

    return (
        <main>
            <Header />
            <main className="page">
                <Route path={route('home')} exact component={HomePage} />
                <Route path={route('period', ':period')} exact component={ForecastPage} />
                <Route path={route('periodByCity', ':city', ':period')} exact component={ForecastPage} />
            </main>
        </main>
    )
}

export default DefaultContainer