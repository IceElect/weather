import { useContext, useEffect } from "react";
import { WeatherContext } from "../../context/Weather/WeatherContext";
import classnames from 'classnames';

const Weather = ({needDesc}) => {    
    const { 
        error, 
        weather,
        location: {city}, 
        loadingWeather,
        fetchWeather,
        
        getTemp,
        getLocation,
        getWeatherDesc,

        favorites,
        toggleFavorite
    } = useContext(WeatherContext)
    
    useEffect(() => {
        if(!loadingWeather && city)
            fetchWeather()
    }, [city, error])

    return (
        <section className="weather section">
            {
                error ? 
                <div className="weather__error">
                    <div>{error}</div>
                </div>
                :
                <div className="weather__container container">
                    <div className="weather__info">
                        {!weather.clouds 
                        ? 
                            <div className="weather__info-loader">
                                <i className="fal fa-spinner fa-spin" />
                            </div> 
                        : 
                        <>
                            <span className="weather__info-temp">{getTemp()}</span>
                            <span className="weather__info-location">{getLocation()}</span>
                            {needDesc && <span className="weather__info-desc">{getWeatherDesc()}</span>}
                        </>
                        }
                    </div>
                    <div 
                        onClick={() => toggleFavorite(city)}
                        className={classnames("favorite__button", {"active": favorites.includes(city)})} 
                    />
                </div>
            }
        </section>
    )
}

export default Weather