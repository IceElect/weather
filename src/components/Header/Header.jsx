import { useContext } from "react"
import { NavLink } from "react-router-dom"
import { WeatherContext } from "../../context/Weather/WeatherContext"
import history from "../../hoc/history"
import route from "../../hoc/route"

const Header = (props) => {

    const { location, period } = useContext(WeatherContext)

    const handleSearchPress = (e) => {
        if (e.key === 'Enter') {
            let city = e.target.value;
            history.push(route('periodByCity', city, 'today'))
        }
    }

    return (
        <header className="header">
            <div className="header__container container">
                <div className="header__inner row">
                    <div className="header__menu col-md-9">
                        <NavLink to={route('periodByCity', location.city, 'today')} className="header__menu-link">Today</NavLink>
                        <NavLink to={route('periodByCity', location.city, 'tomorrow')} className="header__menu-link">Tomorrow</NavLink>
                        <NavLink to={route('periodByCity', location.city, 'week')} className="header__menu-link">Week</NavLink>
                    </div>
                    <div className="header__search col-md-3">
                        <input type="search" className="control control--dark" placeholder="Find city..." onKeyDown={handleSearchPress} />
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header