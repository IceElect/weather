import { useContext } from "react";
import route from '../../hoc/route';
import { periodTypes, WeatherContext } from "../../context/Weather/WeatherContext";
import { Link } from "react-router-dom";

const FavoriteList = () => {

    const { favorites, toggleFavorite } = useContext(WeatherContext)

    return (
        <section className="section">
            <div className="section__container container">
                <div className="section__header">
                    <h3 className="section__header-title">Favorite List</h3>
                    <span className="section__header-subtitle">{favorites.length} items</span>
                </div>
                <div className="section__content">
                    {favorites.length ? 
                    <div className="favorite__list row">
                        {favorites.map((item, key) => (
                            <div className="col-md-3" key={key}>
                                <div className="card favorite__list-item">
                                    <Link to={route('periodByCity', item, periodTypes.today)} className="favorite__list-item-link">
                                        <h3 className="favorite__list-item-name">{item}</h3>
                                    </Link>
                                    <span className="favorite__list-item-button" onClick={() => toggleFavorite(item)}>
                                        <i className="fal fa-fw fa-times"></i>
                                    </span>
                                </div>
                            </div>
                        ))}
                    </div>
                    : <div className="card">You have no favorites cities. You can add any city to your favorites to quickly jump to it next time.</div>
                    }
                </div>
            </div>
        </section>
    )
}

export default FavoriteList