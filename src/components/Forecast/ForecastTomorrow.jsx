import { useContext } from "react";
import { WeatherContext } from "../../context/Weather/WeatherContext";
import Map from "../Map"

const ForecastTomorrow = ({period, coords}) => {

    const { getForecastHourly } = useContext(WeatherContext);

    const forecast = getForecastHourly();

    return (
        <>
            <div className="section__header">
                <h2 className="section__header-title">Tomorrow</h2>
                <span className="section__header-subtitle">{period}</span>
            </div>
            <div className="section__inner row">
                <div className="section__inner-column col-md-6">
                    {coords.lng && <Map coords={coords} isMarkerShown /> }
                </div>
                <div className="section__inner-column col-md-6">
                    <div className="section__content">
                        <table className="card" cellSpacing="10px">
                            <thead>
                                <tr>
                                    <th align="left">Time</th>
                                    <th align="left">Weather</th>
                                </tr>
                            </thead>
                            <tbody>
                                {forecast && forecast.map((item, key) => (
                                    <tr key={key}>
                                        <td>{item.time}</td>
                                        <td><b>{item.temp} °C</b>, {item.weather[0].main}, Wind - {item.wind_speed} meter per second</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ForecastTomorrow