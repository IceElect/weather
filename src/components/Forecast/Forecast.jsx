import { useContext, useEffect } from "react"
import { WeatherContext } from "../../context/Weather/WeatherContext"
import ForecastToday from "./ForecastToday"
import ForecastTomorrow from "./ForecastTomorrow"
import ForecastWeek from "./ForecastWeek"

const Forecast = ({period}) => {
    const { 
        error, 
        location: {city, coords}, 
        loadingForecast,
        fetchForecast,
        getPeriod,
    } = useContext(WeatherContext)
    
    useEffect(() => {
        if(!loadingForecast && city && coords.lat)
            fetchForecast(period)
    }, [city, period, error])

    if(error)
        return ''

    let templates = {
        'today': ForecastToday,
        'tomorrow': ForecastTomorrow,
        'week': ForecastWeek
    }

    let Template = templates[period] || templates.today

    return (
        <section className={`forecast forecast--${period} section`}>
            {
                loadingForecast 
                ? 
                <div className="section__loader">
                    <i className="fal fa-spinner fa-spin" />
                </div> 
                :
                <div className="forecast__container container">
                    <Template period={getPeriod()} coords={coords} />
                </div>
            }
        </section>
    )
}

export default Forecast