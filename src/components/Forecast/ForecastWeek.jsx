import { useContext } from "react";
import { WeatherContext } from "../../context/Weather/WeatherContext";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

const ForecastWeek = ({period, coords}) => {

    const { getForecastDaily } = useContext(WeatherContext);

    const forecast = getForecastDaily();

    const cDate = new Date();
    const weekDay = cDate.getDay();

    const sliderSettings = {
        arrows: true,
        dots: true,
        speed: 500,
        slidesToShow: 6,
        slidesToScroll: 1,
        initialSlide: weekDay - 1,
        infinite: false,
        prevArrow: <span><i className="fal fa-fw fa-chevron-left" /></span>,
        nextArrow: <span><i className="fal fa-fw fa-chevron-right" /></span>,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    };

    return (
        <>
            <div className="section__header">
                <h2 className="section__header-title">Week</h2>
                <span className="section__header-subtitle">{period}</span>
            </div>
            <div className="section__content">
                <div className="forecast__carousel">
                    <Slider {...sliderSettings}>
                        {forecast.map((item, key) => (
                            <div className="forecast__carousel-item" key={key}>
                                <div className="forecast__carousel-item-inner">
                                    <h3 className="forecast__carousel-item-title">{item.weekDayName.substr(0, 2)}</h3>
                                    <span className="forecast__carousel-item-date">{item.date}</span>
                                    <div className="forecast__carousel-item-icon">
                                        <img src="/images/icon.svg" width="100%" alt=""/>
                                    </div>
                                    <div className="forecast__carousel-item-temp">
                                        <span>{(item.temp.min + item.temp.max) / 2 | 0}</span>
                                        <span className="feels">{item.feels_like.day | 0}</span>
                                    </div>
                                    <div className="forecast__carousel-item-desc">{item.weather[0].description}</div>
                                </div>
                            </div>
                        ))}
                    </Slider>
                </div>
            </div>
        </>
    )
}

export default ForecastWeek