"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.prepareHistoryDay = exports.prepareForecastDaily = exports.prepareForecastHourly = exports.weekDayNames = exports.monthNames = void 0;
var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
exports.monthNames = monthNames;
var weekDayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
exports.weekDayNames = weekDayNames;

var prepareForecastHourly = function prepareForecastHourly(forecast, sDate, eDate) {
  var start = sDate.getTime();
  var end = eDate.getTime();
  forecast.hourly = forecast.hourly.map(function (item) {
    if (item.dt * 1000 < start || item.dt * 1000 > end) return false;
    var date = new Date(item.dt * 1000);
    var hours = date.getHours().toString().padStart(2, '0');
    var minutes = date.getMinutes().toString().padStart(2, '0');
    item.time = hours + ':' + minutes;
    return item;
  }).filter(Boolean);
};

exports.prepareForecastHourly = prepareForecastHourly;

var prepareForecastDaily = function prepareForecastDaily(forecast, sDate, eDate) {
  var start = sDate.getTime();
  var end = eDate.getTime();
  forecast.daily = forecast.daily.map(function (item) {
    if (item.dt * 1000 < start || item.dt * 1000 > end) return false;
    var date = new Date(item.dt * 1000);
    var year = date.getYear();
    var day = date.getDate().toString().padStart(2, '0');
    var month = date.getMonth().toString().padStart(2, '0');
    var hours = date.getHours().toString().padStart(2, '0');
    var minutes = date.getMinutes().toString().padStart(2, '0');
    item.time = hours + ':' + minutes;
    item.fullDate = [year, month, day].join('-');
    item.date = "".concat(date.getDate(), " ").concat(monthNames[month].substr(0, 3));
    item.weekDay = date.getDay();
    item.weekDayName = weekDayNames[item.weekDay];
    return item;
  }).filter(Boolean);
};

exports.prepareForecastDaily = prepareForecastDaily;

var prepareHistoryDay = function prepareHistoryDay(day) {
  if (!day.current) return false;
  var minTemp = Infinity,
      maxTemp = -Infinity,
      minFeelsLike = Infinity,
      maxFeelsLike = -Infinity;
  day.hourly.forEach(function (hour) {
    if (hour.temp < minTemp) minTemp = hour.temp;
    if (hour.temp > maxTemp) maxTemp = hour.temp;
    if (hour.feels_like < minFeelsLike) minFeelsLike = hour.temp;
    if (hour.feels_like > maxFeelsLike) maxFeelsLike = hour.temp;
  });
  day.current.temp = {
    min: minTemp,
    max: maxTemp
  };
  day.current.feels_like = {
    day: (minFeelsLike + maxFeelsLike) / 2
  };
  return day.current;
};

exports.prepareHistoryDay = prepareHistoryDay;