export const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
export const weekDayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

export const prepareForecastHourly = (forecast, sDate, eDate) => {

    let start = sDate.getTime();
    let end = eDate.getTime();

    forecast.hourly = forecast.hourly.map((item) => {
        if(item.dt * 1000 < start || item.dt * 1000 > end) 
            return false;

        let date = new Date(item.dt * 1000);
        let hours = date.getHours().toString().padStart(2, '0');
        let minutes = date.getMinutes().toString().padStart(2, '0');

        item.time = hours + ':' + minutes;

        return item;

    }).filter(Boolean)
}

export const prepareForecastDaily = (forecast, sDate, eDate) => {

    const start = sDate.getTime();
    const end = eDate.getTime();

    forecast.daily = forecast.daily.map((item) => {
        if(item.dt * 1000 < start || item.dt * 1000 > end) 
            return false;

        let date = new Date(item.dt * 1000);
        let year = date.getYear();
        let day = date.getDate().toString().padStart(2, '0');
        let month = date.getMonth().toString().padStart(2, '0');
        let hours = date.getHours().toString().padStart(2, '0');
        let minutes = date.getMinutes().toString().padStart(2, '0');

        item.time = hours + ':' + minutes;
        item.fullDate = [year, month, day].join('-');
        item.date = `${date.getDate()} ${monthNames[month].substr(0, 3)}`;

        item.weekDay = date.getDay();
        item.weekDayName = weekDayNames[item.weekDay];

        return item;

    }).filter(Boolean)
}

export const prepareHistoryDay = (day) => {

    if(!day.current) return false;

    let minTemp = Infinity,
        maxTemp = -Infinity,
        minFeelsLike = Infinity,
        maxFeelsLike = -Infinity

    day.hourly.forEach(hour => {
        if(hour.temp < minTemp) minTemp = hour.temp
        if(hour.temp > maxTemp) maxTemp = hour.temp

        if(hour.feels_like < minFeelsLike) minFeelsLike = hour.temp
        if(hour.feels_like > maxFeelsLike) maxFeelsLike = hour.temp
    })

    day.current.temp = {min: minTemp, max: maxTemp}
    day.current.feels_like = {day: (minFeelsLike + maxFeelsLike) / 2}

    return day.current
}