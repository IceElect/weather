import Geocode from "react-geocode";

Geocode.setApiKey("AIzaSyDHKxBTL4FCss1Zoztb4BegpKuqF-08f10");

// set response language. Defaults to english.
Geocode.setLanguage("en");

// set response region. Its optional.
// A Geocoding request with region=es (Spain) will return the Spanish city.
Geocode.setRegion("es");

// Enable or disable logs. Its optional.
Geocode.enableDebug();

export default Geocode